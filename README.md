Quishka: a proof-of-concept of TLS using SSH keys (with QUIC)
=======================

Status: **EXPERIMENTAL**

This software wants to become a library for building arbitrary applications using SSH keys, without getting the SSH protocol involved.

Currently, there's a client and a server which demonstrate rejecting and accepting keys. When successful, they exchange the message: *hello*.

For the "why" and "how", see the blog post at [https://dorotac.eu/blog/ssh-keys](https://dorotac.eu/blog/ssh-keys).

Usage
-----

0. Install Rust and Cargo
1. Generate one ED25519 SSH keypair for the client and one for the server: `ssh-keygen -t ed25519 -f my_key`
2. Run server in one terminal: `cargo run --bin server -- -k server_private_key`
3. Run the client in another terminal: `cargo run --bin client -- -i client_private_key --known_hosts known_test`
4. Note the failure messages, add keys, run again.
5. If all goes fine, watch the "hello" in client's output.

Both the server and the client programs support the `--help` argument on the command line.

Helper programs
--------

- selfsigned: generates a self-signed X.509 certificate for localhost with ECDSA NIST P-256 curve, in DER form. Check with `openssl asn1parse -inform der -in self.crt`
- selfsigned_ed: same, but ED25519
- read: reads the ed25519 SSH key named "foo", and outputs a self-signed certificate named "c" in PEM form. Check with `openssl asn1parse -inform pem -in c`
- pub2cert: same, except you choose the key, and it uses ssh-agent, so you have to `ssh-add my_ssh_id` first. Then `cargo run --bin pub2cert -- -i my_ssh_id`

Tests
----

I really should write some, shouldn't I...

Contact
-----

If you want to talk about this:
- ping me [on Mastodon](https://fosstodon.org/@dcz) @dcz@fosstodon.org
- comment on the [blog post](https://dorotac.eu/blog/ssh-keys).

License
-----

Quishka is distributed under the terms of the AGPL-3.0 license.

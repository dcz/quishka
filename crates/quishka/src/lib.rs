use std::error;
use std::fs;
use std::io;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process;
use tempfile::NamedTempFile;
use thiserror::Error;

pub use ssh_key;

pub fn get_key(path: &Path) -> Result<(Vec<u8>, Vec<u8>), io::Error> {
    //let path = path.to_str().unwrap_or("/");
    let f = NamedTempFile::new()?;
    f.as_file().write_all(&fs::read(path)?)?;
    
    let pathstr = f.path().to_str().unwrap_or("/");
    let _ = process::Command::new("ssh-keygen")
        .args(&[
            "-e",
            "-p",
            "-m", "pem",
            "-f", pathstr,
        ])
        .output()?;
    //let path = format!("{}.pub", path.to_str().unwrap_or("/"));
    Ok((fs::read(f.path())?, Vec::new()))
}


use der::{asn1::{BitString, ContextSpecific, ObjectIdentifier, OctetString}, EncodePem, Sequence};
use der::pem;

#[derive(Clone, Debug, Eq, PartialEq, Sequence)]
pub struct ECKey {
    pub version: u8,
    pub key: OctetString,
    /// This field contains an ASN.1 `OBJECT IDENTIFIER`, a.k.a. OID.
    pub algorithm: ContextSpecific<ObjectIdentifier>,
    pub pkey: ContextSpecific<BitString>,
}

impl pem::PemLabel for ECKey {
    const PEM_LABEL: &'static str = "EC PRIVATE KEY";
}

#[derive(Clone, Debug, Eq, PartialEq, Sequence)]
pub struct PubECKeyDesc {
    pub typ: ObjectIdentifier,
    pub algorithm: ObjectIdentifier,
}


#[derive(Clone, Debug, Eq, PartialEq, Sequence)]
pub struct PubECKey {
    pub desc: PubECKeyDesc,
    pub pkey: ContextSpecific<BitString>,
}


const ID_EC_PUBLIC_KEY: &'static str = "1.2.840.10045.2.1";
const OID_EC_PUBLIC_KEY: x509_cert::spki::ObjectIdentifier
    = x509_cert::spki::ObjectIdentifier::new_unwrap(ID_EC_PUBLIC_KEY);

const PRIME256V1: &'static str = "1.2.840.10045.3.1.7";

#[derive(Error, Debug)]
enum Error {
	#[error("Password read cancelled")]
	PasswordReadCancelled,
}

use std::ffi::OsString;
/*
pub fn ssh_pubkey_to_tls_cert(pubkey: ssh_key::public::EcdsaPublicKey, signer: SshAgent) -> anyhow::Result<()> {
    let tbs = get_tbs(pubkey.as_sec1_bytes());
    
    let mut writer = Vec::new();
    tbs.encode_value(&mut writer).unwrap();
    let tbs_der = writer;
    
    signer.sign(tbs_der)
}*/

pub fn get_pem_for_signing(path: &Path)
    // pem cert, pubkey
    -> anyhow::Result<(Vec<u8>, ssh_key::PublicKey)>
{
    let pubkey = ssh_key::PublicKey::read_openssh_file(&path.with_file_name(&{
        let mut fname: OsString = path.file_name()
            .map(|p| p.to_owned())
            .unwrap_or_else(|| OsString::new());
        fname.push(".pub");
        fname
    })).unwrap();
    
    let tbs = get_tbs(pubkey.key_data());
    let cert = make_cert(tbs, (&pubkey, &SshAgent)).unwrap();
    let cert = cert.to_pem(x509_cert::der::pem::LineEnding::LF).unwrap().into();
    Ok((cert, pubkey))
}

pub fn get_pem(path: &Path) -> anyhow::Result<(Vec<u8>, Vec<u8>)> {
    let k = ssh_key::PrivateKey::read_openssh_file(path)?;
    let k = if k.is_encrypted() {
        use std::env;
        let pass = env::var_os("SSH_ASKPASS")
            .map(|askpass|
                process::Command::new(askpass).output()
            );
        use std::str;
        let pass = match pass {
            Some(Ok(process::Output{status, stdout, ..}))
                if status.code() == Some(0)
            => Some(str::from_utf8(&stdout)?.trim().into()),
            // cancelled
            Some(Ok(process::Output{status, ..}))
                if status.code() == Some(1)
            => None,
            // fallback
            _ => Some(rpassword::prompt_password("Your password: ").unwrap()),
        }.ok_or(Error::PasswordReadCancelled)?;
        k.decrypt(&pass)?
    } else {
        k
    };

    Ok(match k.key_data() {
        ssh_key::private::KeypairData::Ecdsa(ssh_key::private::EcdsaKeypair::NistP256 { public, private }) => {
            // Example parameters value: OID for the NIST P-256 elliptic curve.

            let key = ECKey {
                version: 1,
                key: OctetString::new(private.as_slice()).unwrap(),
                algorithm: ContextSpecific {
                    tag_number: der::TagNumber::N0,
                    tag_mode: der::TagMode::Explicit,
                    value: PRIME256V1.parse().unwrap(),
                },
                pkey: ContextSpecific {
                    tag_number: der::TagNumber::N1,
                    tag_mode: der::TagMode::Explicit,
                    value: BitString::new(0, public.as_bytes()).unwrap(),
                },
            };
            
            (
                key.to_pem(pem::LineEnding::LF).unwrap().into(),
                get_cert(
                    &k.public_key().key_data(),
                    private.as_slice()
                ),
            )
        },
        other => panic!("{:?}", other),
    })
}

const ECDSA_WITH_SHA256: &'static str = "1.2.840.10045.4.3.2";
const ED25519: &'static str = "1.3.101.112";

const OID_ED25519: x509_cert::spki::ObjectIdentifier
    = x509_cert::spki::ObjectIdentifier::new_unwrap(ED25519);

fn get_tbs(public: &ssh_key::public::KeyData)
    -> x509_cert::certificate::TbsCertificate
{
    use std::str::FromStr;
    use x509_cert::{certificate, spki};
    use x509_cert::der::asn1;
    use der::Encode;
    use x509_cert::der::oid::AssociatedOid;
    
    let (sig_oid, subject_public_key_info) = match public {
        &ssh_key::public::KeyData::Ecdsa(public) => (
            ECDSA_WITH_SHA256,
            spki::SubjectPublicKeyInfoOwned {
                algorithm: spki::AlgorithmIdentifierOwned {
                    oid: OID_EC_PUBLIC_KEY,
                    parameters: Some(asn1::Any::from(
                        spki::ObjectIdentifier::new_unwrap(PRIME256V1)
                    )),
                },
                subject_public_key: BitString::new(0, public.as_sec1_bytes()).unwrap(),
            },
        ),
        &ssh_key::public::KeyData::Ed25519(public) => (
            ED25519,
            spki::SubjectPublicKeyInfoOwned {
                algorithm: spki::AlgorithmIdentifierOwned {
                    oid: spki::ObjectIdentifier::new_unwrap(ED25519),
                    parameters: None,
                },
                subject_public_key: BitString::new(
                    0,
                    &public.as_ref()[..],
                ).unwrap(),
            },
        ),
        _ => panic!("Unsupported key type"),
    };
    
    certificate::TbsCertificate {
        version: certificate::Version::V3,
        serial_number: x509_cert::serial_number::SerialNumber::new(&[10; 8]).unwrap(),
        signature: spki::AlgorithmIdentifierOwned {
            oid: spki::ObjectIdentifier::new_unwrap(sig_oid),
            parameters: None,
        },
        issuer: x509_cert::name::Name::from_str("CN=rcgen self signed cert").unwrap(),
        validity: x509_cert::time::Validity {
            not_before: x509_cert::time::Time::GeneralTime(
                asn1::GeneralizedTime::from_date_time(
                    x509_cert::der::DateTime::new(2000, 1, 1, 1, 1, 1)
                        .unwrap()
                )
            ),
            not_after: x509_cert::time::Time::GeneralTime(
                asn1::GeneralizedTime::from_date_time(
                    x509_cert::der::DateTime::new(4000, 1, 1, 1, 1, 1)
                        .unwrap(),
                )
            ),
        },
        subject: x509_cert::name::Name::from_str("CN=rcgen self signed cert").unwrap(),
        subject_public_key_info,
        issuer_unique_id: None,
        subject_unique_id: None,
        // TODO: alternative DNS name
        //X509v3 extensions:
        //    X509v3 Subject Alternative Name: 
          //      DNS:localhost
        extensions: Some(vec![
            x509_cert::ext::Extension {
                extn_id: x509_cert::ext::pkix::SubjectAltName::OID,
                critical: false,
                extn_value: x509_cert::ext::pkix::SubjectAltName(vec![
                    x509_cert::ext::pkix::name::GeneralName::DnsName(
                        asn1::Ia5String::new("localhost").unwrap()
                    )
                ])
                    .to_der().and_then(OctetString::new).unwrap()
            },
        ]),
    }
}


#[derive(Error, Debug)]
pub enum SigError {
    #[error("Signature failed in ring")]
    Ring(ring::error::Unspecified),
    #[error("Unsupported algorithm")]
    Unsupported
}

pub use thrussh_keys::signature::Signature;

pub trait Signer {
    type Error: error::Error + Sync + Send + 'static;
    fn sign(&self, data: &[u8]) -> Result<Signature, Self::Error>;
}

impl<'a> Signer for (&'a ring::signature::EcdsaKeyPair, &'a ring::rand::SystemRandom) {
    type Error = SigError;
    fn sign(&self, data: &[u8]) -> Result<Signature, Self::Error> {
        self.0.sign(self.1, data)
            .map(|sig| Signature::P256(sig.as_ref().to_owned()))
            .map_err(SigError::Ring)
    }
}

use std::thread;
use thrussh_keys::agent::client::AgentClient;
use tokio::runtime::Runtime;

pub struct SshAgent;

impl SshAgent {
    fn sign(&self, key: &ssh_key::PublicKey, data: &[u8])
        -> Result<Signature, thrussh_keys::Error>
    {
        //let key = key.key_data().ecdsa().unwrap().as_sec1_bytes();
        //dbg!(String::from_utf8_lossy(algo.as_bytes()), String::from_utf8_lossy(key));
        //thrussh_keys::parse_public_key_base64("AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBDmt4mv8/EGxVNpzEqkXqOIihUTKWjHa2fUieOBwLTKyv8y8VPmf/BKUuI3A1k3aQjR2NBuB4glem5AgX89mD1Q=").unwrap();
        
        let key = thrussh_keys::parse_public_key_base64(
            key.to_openssh().unwrap()
                .split_ascii_whitespace()
                .skip(1)
                .next().unwrap()
        ).unwrap();
        
        use std::env;
        
        let res = thread::scope(|s| {
            s.spawn(move || {
                let rt  = Runtime::new().unwrap();
                
                rt.block_on(async {
                    let client = tokio::net::UnixStream::connect(
                        env::var_os("SSH_AUTH_SOCK").unwrap_or_default()
                    ).await?;
                    let client = AgentClient::connect(client);
                    Ok(client.sign_request_signature(&key, data).await)
                })
            }).join().unwrap()
        });
        match res {
            Ok((_, Ok(sig @ thrussh_keys::signature::Signature::P256(..))))
                => Ok(sig),
            Ok((_, Ok(sig @ thrussh_keys::signature::Signature::Ed25519(..))))
                => Ok(sig),
            Ok((_, Err(e))) => Err(e),
            Ok((_, Ok(_))) => panic!("Unsupported signature"),
            Err(e) => Err(e),
        }
    }
}

impl Signer for (&ssh_key::PublicKey, &SshAgent) {
    type Error = thrussh_keys::Error;
    fn sign(&self, data: &[u8]) -> Result<Signature, Self::Error> {
        self.1.sign(self.0, data)
    }
}

fn make_cert(
    tbs: x509_cert::TbsCertificate,
    signer: impl Signer,
) -> anyhow::Result<x509_cert::Certificate> {
    use x509_cert::spki;
    use x509_cert::der::{asn1, EncodeValue};
    
    let mut writer = Vec::new();
    tbs.encode_value(&mut writer).unwrap();
    let tbs_der = writer;
    
    let signature = signer.sign(&tbs_der)?;
    
    let (bytes, algo) = match signature {
        Signature::P256(bytes)
            => Ok((bytes, ECDSA_WITH_SHA256)),
        Signature::Ed25519(bytes)
            => Ok((bytes.0[..].into(), ED25519)),
        _ => Err(SigError::Unsupported),
    }?;
    
    Ok(x509_cert::Certificate {
        tbs_certificate: tbs,
        signature_algorithm: spki::AlgorithmIdentifierOwned {
            oid: spki::ObjectIdentifier::new_unwrap(algo),
            parameters: None,
        },
        signature: asn1::BitString
            ::new(0, bytes)
            .unwrap(),
    })
}

pub fn get_cert(public: &ssh_key::public::KeyData, private: &[u8]) -> Vec<u8> {
    let tbs = get_tbs(public);
    
    let s = ring::signature::EcdsaKeyPair::from_private_key_and_public_key(
        &ring::signature::ECDSA_P256_SHA256_ASN1_SIGNING,
        private,
        match public {
            ssh_key::public::KeyData::Ecdsa(public) => public.as_sec1_bytes(),
            _ => panic!(),
        },
    ).unwrap();

    let rng = ring::rand::SystemRandom::new();
    
    let cert = make_cert(tbs, (&s, &rng)).unwrap();
    cert.to_pem(x509_cert::der::pem::LineEnding::LF).unwrap().into()
}

/*
pub fn verify_sig(&rustls::Certificate(cert): &rustls::Certificate) -> bool {
    webpki::EndEntityCert::from(cert)
        .map(|c| verify_signature
}*/

pub fn get_fingerprint(public_sec1_ecdsa: &[u8]) -> Option<[u8; 32]> {
    use ssh_key::sec1;
    let pubk = ssh_key::public::EcdsaPublicKey::NistP256(
        sec1::EncodedPoint::<typenum::U32>::from_bytes(
            public_sec1_ecdsa
        ).ok()?
    );
    let pubf = ssh_key::Fingerprint::new(
        ssh_key::HashAlg::Sha256,
        &ssh_key::public::KeyData::Ecdsa(pubk),
    );
            
    pubf.sha256()
}

pub fn get_sha256_fingerprint(pubkey: &ssh_key::public::KeyData) -> Option<[u8; 32]> {
    let pubf = ssh_key::Fingerprint::new(
        ssh_key::HashAlg::Sha256,
        pubkey,
    );

    pubf.sha256()
}

type EcdsaNistP256PublicKey
    = ssh_key::sec1::EncodedPoint<ssh_key::sec1::consts::U32>;

pub fn get_pubkey(der: &x509_cert::Certificate) -> ssh_key::public::KeyData {
    let k = &der.tbs_certificate.subject_public_key_info.subject_public_key;
    match der.tbs_certificate.subject_public_key_info.algorithm.oid {
        OID_ED25519 => ssh_key::public::KeyData::Ed25519(
            ssh_key::public::Ed25519PublicKey(
                k.as_bytes().unwrap().try_into().unwrap()
            )
        ),
        OID_EC_PUBLIC_KEY => ssh_key::public::KeyData::Ecdsa(
            ssh_key::public::EcdsaPublicKey::NistP256(
                EcdsaNistP256PublicKey::from_bytes(k.raw_bytes())
                    .unwrap(),
            ),
        ),
        _ => panic!("Unknown algorithm"),
    }
}


use std::time::SystemTime;

use x509_cert::der::Decode;


pub type Sha256Fingerprint = [u8; 32];

pub struct MatchFingerprint(pub Sha256Fingerprint);


impl rustls::client::ServerCertVerifier for MatchFingerprint {
    fn verify_server_cert(
        &self,
        end_entity: &rustls::Certificate,
        _intermediates: &[rustls::Certificate],
        _server_name: &rustls::ServerName,
        _scts: &mut dyn Iterator<Item = &[u8]>,
        _ocsp_response: &[u8],
        _now: SystemTime
    ) -> Result<rustls::client::ServerCertVerified, rustls::Error> {
        let c = x509_cert::Certificate::from_der(end_entity.as_ref()).unwrap();
        let pubk = get_pubkey(&c);
        match get_sha256_fingerprint(&pubk) {
            Some(fingerprint) => {
                if fingerprint != self.0 {
                    println!("Unknown fingerprint: {:X?}", fingerprint);
                    Err(rustls::Error::InvalidCertificateData(
                        "Certificate unknown".into()
                    ))
                } else {
                    Ok(rustls::client::ServerCertVerified::assertion())
                }
            },
            None => Err(rustls::Error::CorruptMessage),
        }
    }
}

impl rustls::server::ClientCertVerifier for MatchFingerprint {
    fn client_auth_root_subjects(&self) -> Option<rustls::internal::msgs::handshake::DistinguishedNames> {
        Some(Vec::new())
    }

    fn verify_client_cert(
        &self,
        end_entity: &rustls::Certificate,
        _intermediates: &[rustls::Certificate],
        _now: SystemTime,
    ) -> Result<rustls::server::ClientCertVerified, rustls::Error> {
        let c = x509_cert::Certificate::from_der(end_entity.as_ref()).unwrap();
        let pubk = get_pubkey(&c);
        match get_sha256_fingerprint(&pubk) {
            Some(fingerprint) => {
                if fingerprint != self.0 {
                    println!("Unknown fingerprint: {:X?}", fingerprint);
                    Err(rustls::Error::InvalidCertificateData(
                        "Certificate unknown".into()
                    ))
                } else {
                    Ok(rustls::server::ClientCertVerified::assertion())
                }
            },
            None => Err(rustls::Error::CorruptMessage),
        }
    }
}

#[derive(Debug)]
pub struct CheckKnownHost {
    pub known_hosts: Vec<ssh_key::known_hosts::Entry>,
    pub remote: String,
}

impl rustls::client::ServerCertVerifier for CheckKnownHost {
    fn verify_server_cert(
        &self,
        end_entity: &rustls::Certificate,
        _intermediates: &[rustls::Certificate],
        _server_name: &rustls::ServerName,
        _scts: &mut dyn Iterator<Item = &[u8]>,
        _ocsp_response: &[u8],
        _now: SystemTime
    ) -> Result<rustls::client::ServerCertVerified, rustls::Error> {
        let c = x509_cert::Certificate::from_der(end_entity.as_ref()).unwrap();
        let pubk = get_pubkey(&c);
        let found = self.known_hosts.iter().find(|entry| match entry.host_patterns() {
            ssh_key::known_hosts::HostPatterns::Patterns(names) 
                => names.iter().find(|n| **n == self.remote).is_some(),
            ssh_key::known_hosts::HostPatterns::HashedName { .. } => {
                println!("hashed names not supported yet");
                false
            }
        });
        if let Some(entry) = found {
            let ok = match entry.marker() {
                Some(ssh_key::known_hosts::Marker::Revoked) => false,
                Some(ssh_key::known_hosts::Marker::CertAuthority) => false, // wrong key type
                None => true,
            };
            
            let ok = ok && entry.public_key().key_data() == &pubk;

            if ok {
                Ok(rustls::client::ServerCertVerified::assertion())
            } else {
                Err(rustls::Error::InvalidCertificateData(
                    "Invalid public key for this host".into()
                ))
            }
        } else {
            println!("Unknown pubkey with fingerprint: {:X?}", get_sha256_fingerprint(&pubk).unwrap());
            
            println!(
                "IF YOU TRUST THIS KEY, add this entry to your known_hosts file:
{} {}",
                ssh_key::known_hosts::HostPatterns::Patterns(vec![
                    self.remote.clone(),
                ]).to_string(),
                ssh_key::PublicKey::from(pubk).to_string(),
            );
            Err(rustls::Error::InvalidCertificateData(
                "Public key unknown".into()
            ))
        }
    }
}

#[derive(Debug)]
pub struct CheckAuthorizedKey {
    pub authorized_keys: Vec<ssh_key::authorized_keys::Entry>,
}

impl rustls::server::ClientCertVerifier for CheckAuthorizedKey {
    fn client_auth_root_subjects(&self) -> Option<rustls::internal::msgs::handshake::DistinguishedNames> {
        Some(Vec::new())
    }

    fn verify_client_cert(
        &self,
        end_entity: &rustls::Certificate,
        _intermediates: &[rustls::Certificate],
        _now: SystemTime,
    ) -> Result<rustls::server::ClientCertVerified, rustls::Error> {
        let c = x509_cert::Certificate::from_der(end_entity.as_ref()).unwrap();
        let pubk = get_pubkey(&c);

        let found = self.authorized_keys.iter()
            .find(|entry| entry.public_key().key_data() == &pubk);

        if let Some(_) = found {
            Ok(rustls::server::ClientCertVerified::assertion())
        } else {
            println!(
                "Unknown pubkey{}, rejecting connection.",
                get_sha256_fingerprint(&pubk).map_or_else(
                    || "".into(),
                    |k| format!(" with fingerprint: {:X?}", k),
                )
            );
            println!(
                "IF THIS KEY SHOULD GAIN ACCESS, add this entry to your authorized_keys file:
{}",
                ssh_key::PublicKey::from(pubk).to_string(),
            );
            Err(rustls::Error::InvalidCertificateData(
                "Public key unknown".into()
            ))
        }
    }
}

pub fn expand_path<P: AsRef<Path>>(p: P) -> PathBuf {
    let p: &Path = p.as_ref();
    if p.starts_with("~") {
        dirs::home_dir().map(|h| h.join(p.strip_prefix("~/").unwrap()))
            .unwrap_or(p.to_path_buf())
    } else {
        p.into()
    }
}
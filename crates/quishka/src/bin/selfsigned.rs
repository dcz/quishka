use std::fs;

fn main () {
    let cert = rcgen::generate_simple_self_signed(vec!["localhost".into()]).unwrap();
    let key = cert.serialize_private_key_der();
    let cert = cert.serialize_der().unwrap();
    fs::write(&"self.crt", &cert).expect("failed to write certificate");
    fs::write(&"self.priv", &key).expect("failed to write private key");
}
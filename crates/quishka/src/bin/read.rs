use quishka;
use std::error;
use std::fs;
use std::path::Path;

fn main() {
    if let Err(e) = test() {
        panic!("{}", e);
    }
}

fn test() -> Result<(), Box<dyn error::Error>> {
    let key = Some(Path::new("foo"));
    let (key, cert) = quishka::get_pem(key.unwrap_or(Path::new("/")))?;
    fs::write("o", key)?;
    fs::write("c", cert)?;
    //let key = rustls::PrivateKey(key);
    Ok(())
}

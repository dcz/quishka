use anyhow::Result;
use clap::Parser;
use quinn;
use quishka;
use std::io::BufReader;
use std::net::SocketAddr;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use thiserror::Error;
use tokio;

#[derive(Parser, Debug)]
#[clap(name = "server")]
struct Opt {
    /// SSH private key
    #[clap(short = 'k', long = "key")]
    key: Option<PathBuf>,
    #[clap(long = "authorized_keys", default_value = "~/.ssh/authorized_keys")]
    authorized_keys: PathBuf,
    /// Address to listen on
    #[clap(long = "listen", default_value = "[::1]:4433")]
    listen: SocketAddr,
}

fn main() {
    let opt = Opt::parse();
    if let Err(e) = run(opt) {
        panic!("ERROR: {e}");
    }
}

#[derive(Error, Debug)]
enum Error {
	#[error("No access keys specified, all connections would be rejected")]
	NoAccessKeysFound,
}

#[tokio::main]
async fn run(options: Opt) -> Result<()> {
	let path = &options.key.unwrap_or(Path::new("/").into());
    let (key, cert) = quishka::get_pem(path)?;
	let pkcs8 = rustls_pemfile::ec_private_keys(&mut BufReader::new(&key[..]))?;
	let key = match pkcs8.into_iter().next() {
		Some(key) => rustls::PrivateKey(key),
		_ => panic!(),
	};

	let authorized_keys = quishka::expand_path(options.authorized_keys);
	let authorized_keys = {
		let keys = ssh_key::AuthorizedKeys::read_file(authorized_keys)?;
		if keys.is_empty() {
			Err(Error::NoAccessKeysFound)
		} else {
			Ok(keys)
		}
	}?;
	
    let server_crypto = rustls::ServerConfig::builder()
        .with_safe_defaults()
		.with_client_cert_verifier(Arc::new(
			quishka::CheckAuthorizedKey{ authorized_keys }
		))
        .with_single_cert(
			vec![rustls::Certificate(
				der::pem::decode_vec(&cert).unwrap().1
			)],
			key,
		)?;
    let server_config = quinn::ServerConfig::with_crypto(Arc::new(server_crypto));

	let endpoint = quinn::Endpoint::server(server_config, options.listen)?;
	eprintln!("listening on {}", endpoint.local_addr()?);

	while let Some(conn) = endpoint.accept().await {
		println!("connection incoming");
		let fut = handle_connection(conn);
		tokio::spawn(async move {
			if let Err(e) = fut.await {
				eprintln!("connection failed: {reason}", reason = e.to_string())
			}
		});
	}
	
	Ok(())
}

async fn handle_connection(conn: quinn::Connecting) -> Result<()> {
	let connection = conn.await?;
	println!(
		"connection remote {:?}, proto {:?}", connection.remote_address(),
		connection
			.handshake_data()
			.map_or_else(
				|| "no handshake".into(),
				|d| {
					d
						.downcast::<quinn::crypto::rustls::HandshakeData>().unwrap()
						.protocol
						.map_or_else(
							|| "<none>".into(),
							|x| String::from_utf8_lossy(&x).into_owned()
						)
				},
			 ),
	);
	async {
		println!("established");

		// Each stream initiated by the client constitutes a new request.
		loop {
			let stream = connection.open_uni().await;
			let stream = match stream {
				Err(quinn::ConnectionError::ApplicationClosed { .. }) => {
					println!("connection closed");
					return Ok(());
				}
				Err(e) => {
					return Err(e);
				}
				Ok(s) => s,
			};
			println!("accepted");
			let fut = h(stream);
			tokio::spawn(
				async move {
					if let Err(e) = fut.await {
						eprintln!("failed: {reason}", reason = e.to_string());
					}
				}
			);
		}
	}
	.await?;
	Ok(())
}


async fn h(mut send: quinn::SendStream) -> Result<()> {
	println!("writing");
	send.write_all("hello".as_bytes()).await?;
	println!("written");
	Ok(())
}
use std::fs;

fn main () {
    let mut params = rcgen::CertificateParams::new(vec![
        "localhost".into()
    ]);
    params.alg = &rcgen::PKCS_ED25519;
    let cert = rcgen::Certificate::from_params(params).unwrap();
    let key = cert.serialize_private_key_der();
    let cert = cert.serialize_der().unwrap();
    fs::write(&"ed.crt", &cert).expect("failed to write certificate");
    fs::write(&"ed.priv", &key).expect("failed to write private key");
}
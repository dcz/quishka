use anyhow::Result;
use clap::Parser;
use quishka;
use std::io;
use std::io::Write;
use std::path::{Path, PathBuf};


#[derive(Parser, Debug)]
#[clap(name = "client")]
struct Opt {
    /// Client key
    #[clap(short = 'i', long = "identity")]
    key: Option<PathBuf>,
}


fn main() {
    let opt = Opt::parse();
    if let Err(e) = run(opt) {
        panic!("ERROR: {e}");
    }
}


fn run(options: Opt) -> Result<()> {
    let path = &options.key.unwrap_or(Path::new("/").into());
    
    let (cert, _) = quishka::get_pem_for_signing(path)?; 
    io::stdout().write(&cert)?;
    Ok(())
}
use anyhow::{anyhow, Result};
use clap::Parser;
use quinn;
use quishka;
use quishka::Signer;
use std::net::SocketAddr;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use tokio;


#[derive(Parser, Debug)]
#[clap(name = "client")]
struct Opt {
    /// Client key
    #[clap(short = 'i', long = "identity")]
    key: Option<PathBuf>,
    /// Known hosts
    #[clap(long = "known_hosts", default_value = "~/.ssh/known_hosts")]
    hosts: PathBuf,
    /// Address to connect to
    #[clap(long = "remote", default_value = "[::1]:4433")]
    remote: SocketAddr,
}


fn main() {
    let opt = Opt::parse();
    if let Err(e) = run(opt) {
        panic!("ERROR: {e}");
    }
}


#[tokio::main]
async fn run(options: Opt) -> Result<()> {
    let path = &options.key.unwrap_or(Path::new("/").into());
    
    let (cert, pubkey) = quishka::get_pem_for_signing(path)?; 
    
    let known_hosts = quishka::expand_path(options.hosts);
    
    let client_crypto = rustls::ClientConfig::builder()
        .with_safe_defaults()
        .with_custom_certificate_verifier(Arc::new(
            quishka::CheckKnownHost {
                remote: options.remote.to_string(),
                known_hosts: ssh_key::KnownHosts::read_file(known_hosts)
                    .unwrap_or(Vec::new()),
            }
        ))
        .with_client_cert_resolver(Arc::new(SSHAgent{
            cert: rustls::Certificate(
                der::pem::decode_vec(&cert).unwrap().1
            ),
            pubkey,
        }));
        
    let client_config = quinn::ClientConfig::new(Arc::new(client_crypto));
	let endpoint = quinn::Endpoint::client("[::]:0".parse().unwrap())?;
	eprintln!("connecting from {}", endpoint.local_addr()?);

	let conn = endpoint
        .connect_with(client_config, options.remote, "ssh")?
        .await?;
    println!("opening");
    let recv = conn
        .accept_uni()
        .await
        .map_err(|e| anyhow!("failed to open stream: {}", e))?;

    let out = recv.read_to_end(100).await?;
    println!("{}", String::from_utf8_lossy(&out));
	Ok(())
}

#[derive(Clone)]
struct SSHAgent {
    cert: rustls::Certificate,
    pubkey: quishka::ssh_key::PublicKey,
}

impl SSHAgent {
    fn sig_alg(&self)
        -> (rustls::SignatureAlgorithm, rustls::SignatureScheme)
    {
        match self.pubkey.algorithm() {
            ssh_key::Algorithm::Ecdsa { curve } => (
                rustls::SignatureAlgorithm::ECDSA,
                match curve {
                    ssh_key::EcdsaCurve::NistP256 => rustls::SignatureScheme::ECDSA_NISTP256_SHA256,
                    _ => todo!(),
                },
            ),
            ssh_key::Algorithm::Ed25519 => (
                rustls::SignatureAlgorithm::ED25519, rustls::SignatureScheme::ED25519,
            ),
            _ => todo!(),
        }
    }
}

impl rustls::client::ResolvesClientCert for SSHAgent {
    fn resolve(
        &self,
        _acceptable_issuers: &[&[u8]],
        sigschemes: &[rustls::SignatureScheme],
    ) -> Option<Arc<rustls::sign::CertifiedKey>> {
        let supported = self.sig_alg().1;
        sigschemes.iter()
            .find(|ss| **ss == supported)
            .map(|_| Arc::new(rustls::sign::CertifiedKey::new(
                vec![self.cert.clone()],
                Arc::new(self.clone()),
            )))
    }
    fn has_certs(&self) -> bool {true}
}

impl rustls::sign::SigningKey for SSHAgent {
    fn choose_scheme(&self, offered: &[rustls::SignatureScheme])
        -> Option<Box<dyn rustls::sign::Signer>>
    {
        let supported = self.sig_alg().1;
        offered.iter()
            .find(|s| **s == supported)
            .map(|_| Box::new(self.clone()) as Box<dyn rustls::sign::Signer>)
    }

    fn algorithm(&self) -> rustls::SignatureAlgorithm {
        self.sig_alg().0
    }
}

impl rustls::sign::Signer for SSHAgent {
    fn sign(&self, message: &[u8]) -> Result<Vec<u8>, rustls::Error> {
        (&self.pubkey, &quishka::SshAgent)
            .sign(message)
            .map(|s| match s {
                quishka::Signature::Ed25519(bytes)
                    => bytes.0.into(),
                 quishka::Signature::P256(bytes) => bytes,
                 _ => todo!(),
            })
            .map_err(|_| rustls::Error::EncryptError)
    }
    fn scheme(&self) -> rustls::SignatureScheme {
        self.sig_alg().1
    }
}